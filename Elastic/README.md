
# ELK
## Elasticsearch cluster

### Install ES cluster

* sudo apt-get update && sudo apt-get upgrade -y

* sudo apt install default-jre -y

* java --version

* wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

* sudo apt-get install apt-transport-https

* echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list

* sudo apt-get update && sudo apt-get install elasticsearch

* sudo /bin/systemctl daemon-reload && sudo /bin/systemctl enable elasticsearch.service

* sudo systemctl start elasticsearch.service

### Configure firewall

```
firewall-cmd --zone=public --permanent --add-port=9200-9400/tcp
firewall-cmd --zone=public --permanent --add-port=9200-9400/udp
firewall-cmd --zone=public --permanent --add-source=10.0.3.0/24
firewall-cmd --reload
```

### Config ES cluster
```
vi /etc/elasticsearch/elasticsearch.yml

# ------------------------------------ Node ------------------------------------
node.name: es-node01	        
node.roles: [ master, data ]  
#
# ---------------------------------- Network -----------------------------------
network.host: 10.0.3.11	
http.port: 9200					
#
# ---------------------------------- Cluster -----------------------------------
cluster.name: es_cluster                                             
cluster.initial_master_nodes: ["es-node01","es-node02","es-node03"]  
#
# --------------------------------- Discovery ----------------------------------
discovery.seed_hosts: ["10.0.3.11", "10.0.3.12", "10.0.3.13"]                       
#
# ----------------------------------- Paths ------------------------------------
path.data: /var/lib/elasticsearch 
path.logs: /var/log/elasticsearch 
# ----------------------------------- Memory -----------------------------------
bootstrap.memory_lock: true
```

### Config Elasticseach options
```
vim /etc/default/elasticsearch
MAX_LOCKED_MEMORY=unlimited

-----------------------------------------------------------------
vim /etc/sysctl.conf
vm.max_map_count=262144

-----------------------------------------------------------------
vim /etc/security/limits.conf
elasticsearch - nofile 65536
elasticsearch soft memlock unlimited
elasticsearch hard memlock unlimited

-----------------------------------------------------------------
vim /etc/pam.d/su
session    required   pam_limits.so

-----------------------------------------------------------------
vim /etc/systemd/system/elasticsearch.service.d/override.conf
- OR -
systemctl edit elasticsearch

[Service]
LimitMEMLOCK=infinity
```

### Check node
```
curl -XGET 'http://localhost:9200/_cluster/health?pretty'
curl -XGET 'http://localhost:9200/_cluster/state?pretty'
curl -sS -XGET "http://localhost:9200/_cat/master?pretty" |& tee /tmp/the_master_node_was_here
```
### Create indexes
```
curl -X PUT http://172.31.17.206:9200/syslogs/ -H 'Content-Type: application/json' -d '{
  "settings": {
    "index": {
      "number_of_shards": 3,  
      "number_of_replicas": 2 
    }
  }
}'

curl -X PUT http://172.31.17.206:9200/applogs/ -H 'Content-Type: application/json' -d '{
  "settings": {
    "index": {
      "number_of_shards": 3,  
      "number_of_replicas": 2 
    }
  }
}'


curl -X GET http://localhost:9200/_cat/indices?v
```

## Kibana
### Install Kibana
### Setting Kibana connect with ES cluster
```
# Адрес и порт
server.host: 10.0.3.1
server.port: 8080

# Узлы кластера Elasticsearch
elasticsearch.hosts:
  - http://10.0.3.11:9200
  - http://10.0.3.12:9200
  - http://10.0.3.13:9200

# Частота запросов обновления узлов кластера
# Запрос обновлений в случае сбоя подключения
elasticsearch.sniffInterval: 60000
elasticsearch.sniffOnConnectionFault: true

# Логи Kibana
logging.dest: /var/log/kibana/kibana.log
```
### Setting load balancing accross ES cluster
```
# ------------------------------------ Node ------------------------------------
# Имя узла
node.name: es-nlb01

# Указываем роль Coordinating only
node.master: false
node.data: false
node.ingest: false
#
# ---------------------------------- Cluster -----------------------------------
#
cluster.name: es_cluster  # Имя кластера
#
# --------------------------------- Discovery ----------------------------------
discovery.seed_providers: file                       
#
# ---------------------------------- Network -----------------------------------
#
network.host: 10.0.3.1	          # Адрес узла
http.port: 9200					          # Порт
transport.host: 10.0.3.1          # Адрес для связи с кластером
transport.tcp.port: 9300-9400     # Порты для коммуникации внутри кластера
#
# ----------------------------------- Paths ------------------------------------
#
path.data: /var/lib/elasticsearch # Директория с данными
path.logs: /var/log/elasticsearch # Директория с логами
```

```
elasticsearch.hosts:
  - http://10.0.3.1:9200
```
### Add additional node Kibana
```

```


## Logstash
### Install Logstash
### Configure Logstash
### Setting logstash for self syslog

## ELK secure
### ES SSL/TLS
### Kibana SSL/TLS
### Role and users