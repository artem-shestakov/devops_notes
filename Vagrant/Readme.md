# Vagrant
### Simple Ubuntu and CentOS example
```
Vagrant.configure("2") do |config|
 
  # First VM  
  config.vm.define "ubuntu" do |ubuntu|
    ubuntu.vm.box = "ubuntu/bionic64"
    ubuntu.vm.network "private_network", ip: "192.168.100.11", name: "vboxnet0"
    ubuntu.vm.provider "virtualbox" do |vb|
      vb.memory = 2048  # VM memory
      vb.cpus = 1       # VM CPUs

      # Change default audiocontroller
      vb.customize ["modifyvm", :id, "--audiocontroller", "sb16"]
    end
  end
  
  # Second VM
  config.vm.define "centos" do |centos|
    centos.vm.box = "centos/7"
    centos.vm.network "private_network", ip: "192.168.100.12", name: "vboxnet0"
    centos.vm.provider "virtualbox" do |vb|
      vb.memory = 2048  # VM memory
      vb.cpus = 1       # VM CPUs

      # Change default audiocontroller
      vb.customize ["modifyvm", :id, "--audiocontroller", "sb16"]
    end
  end

end
```