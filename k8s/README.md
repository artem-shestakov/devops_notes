# Content
1. [Installation Kubernetes by Kubeadm](#Installation)  
1.1 [Disable SWAP](#swap)  
1.2 [Letting iptables see bridged traffic](#iptable)  
1.3 [Installing containerd runtime](#install_containerd)  
1.3.1 [Install and configure prerequisites](#prerequisites)  
1.3.2 [Install containerd](#containerd)  
1.3.3 [Installing kubeadm, kubelet and kubectl](#install_kube)  
1.3.4 [Configure cgroup driver used by kubelet on control-plane node](#cgroup)  
2. [Create master node](#master_node)  
2.1 [Initialize master](#init_master)  
2.2 [Copy config files for regular user](#regular_user)  
2.3 [Check cluster info](#cluster_info)  
2.4 [Installing a Pod network add-on](#pod_network)  
3. [Add nodes to cluster](#add_node)  
3.1 [Install packages](#packages)  
3.2 [Run join command](#join)  
---

# Installation Kubernetes by Kubeadm <a name="Installation"></a>
## Disable SWAP <a name='swap'></a>
1. Disable SWAP by CLI
```
$ sudo sudo swapoff -a
```
2. Delete SWAP entry from `fstab`
3. Reboot system
```
$ sudo systemctl reboot 
```

## Letting iptables see bridged traffic <a name='iptable'></a>
> ⚠️ More detail look for [official documentation](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)   

1. Make sure that the br_netfilter module is loaded.
```
$ sudo lsmod | grep br_netfilter
br_netfilter           28672  0
bridge                176128  1 br_netfilter
```
To load it explicitly call `sudo modprobe br_netfilter`  

2. Ensure `net.bridge.bridge-nf-call-iptables` is set to 1 in your `sysctl` config
```
$ cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
$ sudo sysctl --system
```  
## Installing containerd runtime <a name="install_containerd"></a>
1. ### Install and configure prerequisites <a name='prerequisites'></a>
```
$ cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

$ sudo modprobe overlay
$ sudo modprobe br_netfilter
```  
2. ### Install containerd <a name='containerd'></a>

* Install packages to allow apt to use a repository over HTTPS
```
$ sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
```

* Add Docker's official GPG key
```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key --keyring /etc/apt/trusted.gpg.d/docker.gpg add -
```

* Add Docker apt repository.
```
$ sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
```

* Install containerd
```
$ sudo apt-get update && sudo apt-get install -y containerd.io
```

* Configure containerd
```
$ sudo mkdir -p /etc/containerd
$ sudo containerd config default | sudo tee /etc/containerd/config.toml
```

* Restart containerd
```
$ sudo systemctl restart containerd
```  

3. ### Installing kubeadm, kubelet and kubectl <a name='install_kube'></a>

* Add Google repo gpg key
```
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```

* Add repository and update package list
```
$ cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

$ sudo apt-get update
```

* Check package version
```
$ apt-cache policy kubelet | head -n 20
$ apt-cache policy kubeadm | head -n 20
```
> ⚠️ `kubeadm` will not install or manage `kubelet` or `kubectl` for you, so you will need to ensure they match the version of the Kubernetes control plane you want `kubeadm` to install for you

* Install packages
```
$ sudo apt-get install -y kubelet kubeadm kubectl
$ sudo apt-mark hold kubelet kubeadm kubectl
```

* Enable services
```
$ sudo systemctl enable kubelet.service
$ sudo systemctl enable containerd.service
```  

4. ### Configure cgroup driver used by kubelet on control-plane node <a name='cgroup'></a>

> ⚠️ When using Docker, kubeadm will automatically detect the cgroup driver for the kubelet and set it in the `/var/lib/kubelet/config.yaml` file during runtime  
> Default cgroup driver is `cgroupfs`

* For `systemd` change /var/lib/kubelet/config.yaml by adding `cgroupDriver`
```
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
```
* For containerd use the `systemd` cgroup driver in `/etc/containerd/config.toml` with `runc`
```
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
    SystemdCgroup = true
```

* Restarting the kubelet
```
$ sudo systemctl daemon-reload
$ sudo systemctl restart kubelet
```  

## Create master node <a name='master_node'></a>

1. ### Initialize master <a name='init_master'></a>
* If you have plans to upgrade this single control-plane kubeadm cluster to high availability you should specify the `--control-plane-endpoint` to set the shared endpoint for all control-plane nodes
* Choose a Pod network by `--pod-network-cidr`  
* (Optional) Detect the container runtime `--cri-socket`
* (Optional) Set the advertise address for this particular control-plane node's API server `--apiserver-advertise-address`
```
$ sudo kubeadm init --pod-network-cidr=192.168.0.0/16 --apiserver-advertise-address 10.0.1.11
```
2. ### Copy config files for regular user (k8s administartor) <a name='regular_user'></a>
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

>⚠️ If you are the root user, you can run:
>```
>export KUBECONFIG=/etc/kubernetes/admin.conf
>``` 

3. ### Check cluster info <a name=cluster_info></a>
```
$ kubectl cluster-info
Kubernetes control plane is running at https://10.0.1.11:6443
KubeDNS is running at https://10.0.1.11:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```
4. ### Installing a Pod network add-on <a name=pod_network></a>
     
* Install the Tigera Calico operator and custom resource definitions
```
$ kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml
```

* Install Calico by creating the necessary custom resource  
>⚠️ Before creating this manifest, read its contents and make sure its settings are correct for your environment
```
$ kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml
```
* Confirm that all of the pods are running
```
watch kubectl get pods -n calico-system
```

* Remove the taints on the master  
```
kubectl taint nodes --all node-role.kubernetes.io/master-
```
## Add nodes to cluster <a name='add_node'></a>
1. ### Install packages <a name='packages'></a>
    Use [instruction](#Installation) for master node. 
2. ### Run join command <a name='join'></a>
```
kubeadm join 10.0.1.11:6443 --token 9gi7e7.65ucunx9xuzf2w5q \
    --discovery-token-ca-cert-hash sha256:4b429824469193c2b75b5b30794af73b2c679e9b7bb89b99faaa2b146a6f0ec3
```

# Kubernetes architecture
## Etcd

`etcdctl` version 2:
```
etcdctl backup
etcdctl cluster-health
etcdctl mk
etcdctl mkdir
etcdctl set
```
`etcdctl` version 3
```
etcdctl snapshot save 
etcdctl endpoint health
etcdctl get
etcdctl put
```

To set the right version of API set the environment variable:
```
export ETCDCTL_API=3
```

`Etcd` certificates:
```
--cacert /etc/kubernetes/pki/etcd/ca.crt     
--cert /etc/kubernetes/pki/etcd/server.crt     
--key /etc/kubernetes/pki/etcd/server.key
```

Example. Geting `Etcd` keys
```
kubectl exec etcd-master -n kube-system -- sh -c "ETCDCTL_API=3 etcdctl get / --prefix --keys-only --limit=10 --cacert /etc/kubernetes/pki/etcd/ca.crt --cert /etc/kubernetes/pki/etcd/server.crt  --key /etc/kubernetes/pki/etcd/server.key"
```

## API server
View API server config for kubeadm look for:
```
cat /etc/kubernetes/manifests/kube-apiserver.yaml
```