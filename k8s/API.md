# API
## Access with Token
```bash
$ APISERVER=$(kubectl config view | grep server | awk '{print $2}')

$ TOKEN=$(kubectl get secrets -o jsonpath="{.items[?(@.metadata.annotations['kubernetes\.io/service-account\.name']=='default')].data.token}"|base64 --decode)

$ curl -k -H "Authorization: Bearer $TOKEN" $APISERVER/api | jq
```
## Acces with Proxy
```bash
$ kubectl proxy
$ curl http://127.0.0.1:8001/api
```